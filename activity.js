// Task 1

db.fruits.aggregate([
	{
		$match: {
			"onSale": true
		}
	}, 
        {
                $group: {
                    _id: "$onSale", fruitsOnsale: {$sum: 1}
                }
        }
])


// Task 2

db.fruits.aggregate([
	{
		$match: {
			"stocks": {$gt: 20}
		}
	}, 
        {
                $group: {
                    _id: "$onSale", enoughStock: {$sum: 1}
                }
        }
])

// Task 3

db.fruits.aggregate([ 
	{
		$match: {
				"onSale": true
		}
	},
	{
        $group: {
            _id: "$supplier", averagePrice: {$avg: "$price"}
        }
    }
])

// Task 4

db.fruits.aggregate([ 
	{
		$match: {
				"onSale": true
		}
	},
	{
        $group: {
            _id: "$supplier", maxPrice: {$max: "$price"}
        }
    }
])

// Task 5 

db.fruits.aggregate([ 
	{
		$match: {
				"onSale": true
		}
	},
	{
        $group: {
            _id: "$supplier", minPrice: {$min: "$price"}
        }
    }
])


